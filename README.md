## Descrição dos Arquivos

Arquivos referente ao deploy dos componentes básicos do Istio.

1. 1-istio-system.yaml -> Criação do namespace istio-system e CustomResourceDefinition para Istio e Kiali;
2. 2-istio-componentes.yaml -> Criação de Services, RBAC e configMap para Kiali, Prometheus, Jaeger, Zipkin, Istio-EgressGateway e Istio-IngressGateway;
3. 3-kiali-secret.yaml -> Criação de senha de acesso ao Kiali
4. 4-label-default-ns -> Aplica istio-injection=enabled para sidecar-proxy

##  Install Flagger

kubectl version --output yaml

# Must have `kubectl` v1.14+

kubectl apply \
    --kustomize github.com/weaveworks/flagger/kustomize/istio



